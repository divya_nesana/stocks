package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Stocks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
      
    private String stockTicker;
    private double price;
    private int volume;
    private String buyOrSell;
    private int stockStatus;
    
    public Stocks() { }
    
	public Stocks(String stockTicker, double price, int volume, String buyOrSell, int stockStatus) {
		super();
		this.stockTicker = stockTicker;
		this.price = price;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.stockStatus = stockStatus;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getStockTicker() {
		return stockTicker;
	}
	
	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getVolume() {
		return volume;
	}
	
	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	public String getBuyOrSell() {
		return buyOrSell;
	}
	
	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}
	
	public int getStockStatus() {
		return stockStatus;
	}
	
	public void setStockStatus(int stockStatus) {
		this.stockStatus = stockStatus;
	}

}
